[![Build Status](https://drone.io/bitbucket.org/patrykpacewicz/wmid-api-security/status.png)](https://drone.io/bitbucket.org/patrykpacewicz/wmid-api-security/latest)

WMID - Api - Security
=====================
Api is created for classes of security protocols

Api Endpoints
-------------

 * POST /secret-sharing/decode
```
curl -v -XPOST -H "Content-Type: application/json" -H "Accept: application/json" localhost:8882/api/secret-sharing/decode -d '{"data":["kpFqSio=","+vQGJkU="]}'
```

 * POST /secret-sharing/encode
```
curl -v -XPOST -H "Content-Type: application/json" -H "Accept: application/json" localhost:8882/api/secret-sharing/encode -d '{"message":"hello","number":2}'
```

 * POST /commitment/symmetric/send
```
curl -v -XPOST -H "Content-Type: application/json" -H "Accept: application/json" localhost:8882/api/commitment/symmetric/send -d '{"clientHost":"http://localhost:8882/api", "message":"xxxx", "key":"key"}'
```

 * POST /commitment/symmetric/validate
```
curl -v -XPOST -H "Content-Type: application/json" -H "Accept: application/json" localhost:8882/api/commitment/symmetric/validate -d '{"message":"xxxx", "key":"key", "id":"52781534e4b0fc2fa51bca02", "encodedMessage":"vdTRemYU+JI="}'
```

 * GET /commitment/symmetric
```
curl -v -H "Accept: application/json" localhost:8882/api/commitment/symmetric
```

Launch for development
----------------------
Application is connected to the vagrant project,
which will prepare the virtual machine and configure it for proper working.
```
vagrant up
```
Enjoy [http://localhost:8882/](http://localhost:8882/)  :-)

What's inside ?
---------------
 * Jersey + Guice
 * ./gradlew (build, war, jettyRun, test, check)
 * Puppet for server configuration
