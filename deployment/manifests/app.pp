exec { 'build war':
  command     => "gradlew war",
  path        => ['/bin', '/usr/bin', $cfg_rootDir],
  cwd         => $cfg_rootDir,
  logoutput   => true,
  require     => Service['jetty']
}

exec { 'cargo redeploy':
  command     => "gradlew cargoRedeployRemote",
  path        => ['/bin', '/usr/bin', $cfg_rootDir],
  cwd         => $cfg_rootDir,
  logoutput   => true,
  require     => Exec['build war'],
}
