include apt

apt::ppa { 'ppa:webupd8team/java': }

exec { 'java-licence-selected':
  command => '/bin/echo debconf shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections',
}

exec { 'java-licence-seen':
  command => '/bin/echo debconf shared/accepted-oracle-license-v1-1 seen true | /usr/bin/debconf-set-selections',
  require => Exec['java-licence-selected']
}

package { 'oracle-java8-installer':
  require => [Apt::Ppa['ppa:webupd8team/java'], Exec['java-licence-selected'], Exec['java-licence-seen']]
}
