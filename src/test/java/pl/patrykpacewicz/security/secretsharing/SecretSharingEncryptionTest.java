package pl.patrykpacewicz.security.secretsharing;

import static org.fest.assertions.Assertions.assertThat;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.Random;

import pl.patrykpacewicz.security.secretsharing.model.Decrypted;
import pl.patrykpacewicz.security.secretsharing.model.Encrypted;
import pl.patrykpacewicz.security.util.RandomGenerator;
import pl.patrykpacewicz.security.util.Xor;


public class SecretSharingEncryptionTest {
    private SecretSharingEncryption encryption;

    @Before
    public void setUp() {
        encryption = new SecretSharingEncryption(new Xor(), new RandomGenerator(new Random()));
    }

    @Test
    public void shouldGenerateMessageSecureParts() {
        Decrypted decrypted = new Decrypted("data to encode", 3);
        Encrypted encrypted1 = encryption.encode(decrypted);
        Encrypted encrypted2 = encryption.encode(decrypted);

        assertThat(encrypted1).isNotEqualTo(encrypted2);
        assertThat(encrypted1.getData()).hasSize(3);
        assertThat(encrypted1.getData().get(0)).hasSize(decrypted.getMessage().length());
        assertThat(encrypted1.getData().get(1)).hasSize(decrypted.getMessage().length());
        assertThat(encrypted1.getData().get(2)).hasSize(decrypted.getMessage().length());
    }

    @Test
    public void shouldMergeEncodedPartsToMessage() throws UnsupportedEncodingException {
        Encrypted encrypted = new Encrypted(
            Lists.newArrayList(
                new byte[] {1, -73, 112, 20, 108, -119},
                new byte[] {114, -46, 19, 102, 9, -3}
            )
        );

        Decrypted decrypted = encryption.decode(encrypted);

        assertThat(decrypted.getMessage()).isEqualTo("secret");
        assertThat(decrypted.getNumber()).isEqualTo(2);
    }

    @Test
    public void shouldEncodeAndDecodeMessage() throws UnsupportedEncodingException {
        Decrypted decrypted = new Decrypted("secret secret data", 5);

        Decrypted decryptedResult = encryption.decode(encryption.encode(decrypted));

        assertThat(decryptedResult.getMessage()).isEqualTo(decrypted.getMessage());
        assertThat(decryptedResult.getNumber()).isEqualTo(decrypted.getNumber());
    }
}
