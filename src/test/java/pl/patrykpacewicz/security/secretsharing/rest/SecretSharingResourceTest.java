package pl.patrykpacewicz.security.secretsharing.rest;

import static org.fest.assertions.Assertions.*;
import static org.mockito.Mockito.*;

import com.google.common.collect.Lists;
import org.junit.*;
import pl.patrykpacewicz.security.secretsharing.SecretSharingEncryption;
import pl.patrykpacewicz.security.secretsharing.model.Decrypted;
import pl.patrykpacewicz.security.secretsharing.model.Encrypted;

import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;

public class SecretSharingResourceTest {
    private SecretSharingEncryption encryption;
    private SecretSharingResource resource;

    @Before
    public void setUp() {
        encryption = mock(SecretSharingEncryption.class);
        resource = new SecretSharingResource(encryption);
    }

    @Test
    public void shouldEncodeDecryptedDataInResponse() {
        Decrypted decrypted = new Decrypted("message", 2);
        Encrypted encrypted = new Encrypted(Lists.newArrayList(new byte[]{}));

        when(encryption.encode(any(Decrypted.class))).thenReturn(encrypted);

        Response response = resource.encode(decrypted);
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(response.getEntity()).isEqualTo(encrypted);
    }

    @Test
    public void shouldDecodeEncryptedDataInResponse() throws UnsupportedEncodingException {
        Encrypted encrypted = new Encrypted(Lists.newArrayList(new byte[]{}));
        Decrypted decrypted = new Decrypted("message", 2);

        when(encryption.decode(any(Encrypted.class))).thenReturn(decrypted);

        Response response = resource.decode(encrypted);
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(response.getEntity()).isEqualTo(decrypted);
    }

}
