package pl.patrykpacewicz.security.secretsharing.model;

import static org.fest.assertions.Assertions.assertThat;

import com.google.common.collect.Lists;
import org.junit.Test;
import java.util.ArrayList;

public class EncryptedTest {
    @Test
    public void testGetData() {
        ArrayList<byte[]> anyList = Lists.newArrayList(new byte[]{1, 2, 3});
        Encrypted encrypted = new Encrypted(anyList);

        assertThat(encrypted.getData()).isEqualTo(anyList);
    }
}
