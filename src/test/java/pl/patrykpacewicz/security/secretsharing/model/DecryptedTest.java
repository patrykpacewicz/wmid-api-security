package pl.patrykpacewicz.security.secretsharing.model;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

public class DecryptedTest {
    @Test
    public void testGetMessage() {
        Decrypted decrypted = new Decrypted("test message", 1);
        assertThat(decrypted.getMessage()).isEqualTo("test message");
    }

    @Test
    public void testGetNumber() {
        Decrypted decrypted = new Decrypted("any", 942);
        assertThat(decrypted.getNumber()).isEqualTo(942);
    }
}
