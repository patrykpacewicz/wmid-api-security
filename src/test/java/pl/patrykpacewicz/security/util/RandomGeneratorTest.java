package pl.patrykpacewicz.security.util;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;
import java.util.List;
import java.util.Random;


public class RandomGeneratorTest {
    @Test
    public void shouldGenerateRandomListOfBytes() {
        RandomGenerator randomGenerator = new RandomGenerator(new Random());
        List<byte[]> randomBytes1 = randomGenerator.generateBytesList(15, 3);
        List<byte[]> randomBytes2 = randomGenerator.generateBytesList(15, 3);

        assertThat(randomBytes1).isNotEqualTo(randomBytes2);
        assertThat(randomBytes1).hasSize(3);
        assertThat(randomBytes1.get(0)).hasSize(15).isNotEqualTo(randomBytes1.get(1));
        assertThat(randomBytes1.get(1)).hasSize(15).isNotEqualTo(randomBytes1.get(2));
        assertThat(randomBytes1.get(2)).hasSize(15).isNotEqualTo(randomBytes1.get(0));
    }

}
