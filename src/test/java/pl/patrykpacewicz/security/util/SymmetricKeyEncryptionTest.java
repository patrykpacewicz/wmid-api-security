package pl.patrykpacewicz.security.util;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class SymmetricKeyEncryptionTest {
    private SymmetricKeyEncryption crypt;

    @Before
    public void setUp() throws NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher cipher = Cipher.getInstance("Blowfish");
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        crypt = new SymmetricKeyEncryption(cipher, digest);
    }

    @Test
    public void shouldEncodeMessageWithSymmetricKey()
            throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException
    {
        String key = "secret key";
        String message = "msg";
        byte[] encodedExpectedData = {-59, -68, 69, -27, 67, -79, 68, 19};

        byte[] data = crypt.encode(message, key);

        assertThat(data).isEqualTo(encodedExpectedData);
    }

    @Test
    public void shouldDecodeDataWithSymmetricKey()
            throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException
    {
        String key = "secret key";
        byte[] encodedData = {-59, -68, 69, -27, 67, -79, 68, 19};
        String expectedMessage = "msg";

        String message = crypt.decode(encodedData, key);

        assertThat(message).isEqualTo(expectedMessage);
    }

    @Test
    public void shouldEncodeAndDecodeMessage()
            throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException
    {
        String key = "super secret key";
        String message = "message to encode/decode";

        byte[] data = crypt.encode(message, key);
        String result = crypt.decode(data, key);

        assertThat(result).isEqualTo(message);
    }
}
