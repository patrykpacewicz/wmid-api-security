package pl.patrykpacewicz.security.util;

import static org.fest.assertions.Assertions.assertThat;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

public class XorTest {
    @Test
    public void shouldXorTwoBytesArray() {
        Xor xor = new Xor();
        byte[] data1 = {1, 2};
        byte[] data2 = {3, 4};
        byte[] expected = {2, 6};

        assertThat(xor.xor(data1, data2)).isEqualTo(expected);
    }

    @Test
    public void shouldXorTwoBytesArrayWithDifferentSizes() {
        Xor xor = new Xor();
        byte[] data1 = {1, 1, 1, 1};
        byte[] data2 = {2, 3};
        byte[] result = {3, 2, 3, 2};

        assertThat(xor.xor(data1, data2)).isEqualTo(result);
    }

    @Test
    public void shouldXorTwoBytesArrayWithoutOrderMatter() {
        Xor xor = new Xor();
        byte[] data1 = {1, 1, 1};
        byte[] data2 = {2, 2};

        byte[] result1 = xor.xor(data1, data2);
        byte[] result2 = xor.xor(data2, data1);

        assertThat(result1).isEqualTo(result2);
    }

    @Test
    public void shouldXorListOfBytes() {
        Xor xor = new Xor();
        byte[] result = {7, 5, 7};
        List<byte[]> data = Lists.newArrayList();

        data.add(new byte[]{1, 2, 3});
        data.add(new byte[]{2, 2, 2});
        data.add(new byte[]{4, 5, 6});

        assertThat(xor.xor(data)).isEqualTo(result);
    }
}
