package pl.patrykpacewicz.security.blindsignature;

import org.junit.Before;
import org.junit.Test;
import pl.patrykpacewicz.security.blindsignature.generator.RsaBlindKeyGenerator;
import pl.patrykpacewicz.security.blindsignature.model.RsaBlindKey;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import static org.fest.assertions.Assertions.assertThat;

public class BlindSignatureProtocolTest {
    private RsaBlindKey rsaBlindKey;

    @Before
    public void setUp() throws NoSuchAlgorithmException {
        rsaBlindKey = new RsaBlindKeyGenerator(new SecureRandom()).generate();
    }

    @Test
    public void shouldVerifySignedMessage() {
        BlindSignatureProtocol blindSignatureProtocol = new BlindSignatureProtocol();
        byte[] message = "Super secret message".getBytes();

        byte[] blindMessage       = blindSignatureProtocol.blindMessage(message, rsaBlindKey);
        byte[] blindSignedMessage = blindSignatureProtocol.sign(blindMessage, rsaBlindKey.getRsaPrivateKey());
        byte[] signedMessage      = blindSignatureProtocol.unblindMessage(blindSignedMessage, rsaBlindKey);

        assertThat(
            blindSignatureProtocol.verifySignedMessage(message, signedMessage, rsaBlindKey.getRsaPublicKey())
        ).isTrue();
    }


    @Test
    public void shouldNotVerifySignedMessageWhenIsInvalid() {
        BlindSignatureProtocol blindSignatureProtocol = new BlindSignatureProtocol();
        byte[] message = "Super secret message".getBytes();

        byte[] blindMessage       = blindSignatureProtocol.blindMessage(message, rsaBlindKey);
        byte[] blindSignedMessage = blindSignatureProtocol.sign(blindMessage, rsaBlindKey.getRsaPrivateKey());
        byte[] signedMessage      = blindSignatureProtocol.unblindMessage(blindSignedMessage, rsaBlindKey);

        signedMessage[0]++;

        assertThat(
            blindSignatureProtocol.verifySignedMessage(message, signedMessage, rsaBlindKey.getRsaPublicKey())
        ).isFalse();
    }
}
