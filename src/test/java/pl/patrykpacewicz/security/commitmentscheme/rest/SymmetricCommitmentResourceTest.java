package pl.patrykpacewicz.security.commitmentscheme.rest;

import static org.fest.assertions.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.*;
import org.springframework.data.mongodb.core.MongoOperations;
import pl.patrykpacewicz.security.commitmentscheme.SymmetricProtocol;
import pl.patrykpacewicz.security.commitmentscheme.document.SymmetricDocument;
import pl.patrykpacewicz.security.commitmentscheme.model.EncodedMessage;
import pl.patrykpacewicz.security.commitmentscheme.model.StartMessage;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.ws.rs.core.Response;
import java.security.InvalidKeyException;

public class SymmetricCommitmentResourceTest {
    private SymmetricProtocol protocol;
    private SymmetricCommitmentResource resource;
    private MongoOperations mongo;

    @Before
    public void setUp() {
        protocol = mock(SymmetricProtocol.class);
        mongo    = mock(MongoOperations.class);
        resource = new SymmetricCommitmentResource(protocol, mongo);
    }

    @Test
    public void shouldSendEcodedMessage() throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException {
        SymmetricDocument symmetricDocument = new SymmetricDocument(new byte[]{});
        StartMessage startMessage = new StartMessage("anyHost", "anyMessage", "anyKey");

        when(protocol.sendEcodedMessage(any(StartMessage.class))).thenReturn(symmetricDocument);

        Response response = resource.send(startMessage);

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(response.getEntity()).isEqualTo(symmetricDocument);
    }

    @Test
    public void shouldSaveEcodedMessage() {
        EncodedMessage encodedMessage = new EncodedMessage(new byte[]{});
        SymmetricDocument symmetricDocument = new SymmetricDocument(new byte[]{});

        when(protocol.saveEcodedMessage(encodedMessage)).thenReturn(symmetricDocument);

        Response response = resource.receive(encodedMessage);

        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(response.getEntity()).isEqualTo(symmetricDocument);
    }
}
