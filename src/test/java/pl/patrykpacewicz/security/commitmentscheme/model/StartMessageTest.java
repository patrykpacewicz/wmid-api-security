package pl.patrykpacewicz.security.commitmentscheme.model;

import static org.fest.assertions.Assertions.*;

import org.junit.Test;

public class StartMessageTest {
    @Test
    public void shouldGetKey() {
        StartMessage startMessage = new StartMessage("anyHost", "anyMessage", "anyKey");
        assertThat(startMessage.getClientHost()).isEqualTo("anyHost");
    }

    @Test
    public void shouldGetMessage() {
        StartMessage startMessage = new StartMessage("anyHost", "anyMessage", "anyKey");
        assertThat(startMessage.getMessage()).isEqualTo("anyMessage");
    }

    @Test
    public void shouldGetClientHost() {
        StartMessage startMessage = new StartMessage("anyHost", "anyMessage", "anyKey");
        assertThat(startMessage.getKey()).isEqualTo("anyKey");
    }
}
