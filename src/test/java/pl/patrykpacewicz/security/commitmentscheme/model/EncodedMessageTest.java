package pl.patrykpacewicz.security.commitmentscheme.model;

import static org.fest.assertions.Assertions.*;

import org.junit.*;

public class EncodedMessageTest {
    @Test
    public void shouldGetData() {
        byte[] data = {};
        EncodedMessage encodedMessage = new EncodedMessage(data);

        assertThat(encodedMessage.getData()).isEqualTo(data);
    }
}
