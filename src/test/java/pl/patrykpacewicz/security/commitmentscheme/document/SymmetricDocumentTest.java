package pl.patrykpacewicz.security.commitmentscheme.document;

import static org.fest.assertions.Assertions.*;

import org.junit.Test;

public class SymmetricDocumentTest {
    @Test
    public void shouldGetEcodedMessage() {
        byte[] data = {};
        SymmetricDocument document = new SymmetricDocument(data);

        assertThat(document.getEncodedMessage()).isEqualTo(data);
    }
}
