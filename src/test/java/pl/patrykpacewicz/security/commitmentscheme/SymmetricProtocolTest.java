package pl.patrykpacewicz.security.commitmentscheme;

import static org.mockito.Mockito.*;

import org.junit.*;
import org.springframework.data.mongodb.core.MongoOperations;
import java.security.InvalidKeyException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;

import pl.patrykpacewicz.security.commitmentscheme.document.SymmetricDocument;
import pl.patrykpacewicz.security.commitmentscheme.model.EncodedMessage;
import pl.patrykpacewicz.security.commitmentscheme.model.StartMessage;
import pl.patrykpacewicz.security.util.SymmetricKeyEncryption;

public class SymmetricProtocolTest {
    private SymmetricProtocol protocol;
    private Client client;
    private SymmetricKeyEncryption symmetricEncryption;
    private MongoOperations mongo;

    @Before
    public void setUp() {
        client              = mock(Client.class);
        symmetricEncryption = mock(SymmetricKeyEncryption.class);
        mongo          = mock(MongoOperations.class);

        protocol = new SymmetricProtocol(client, symmetricEncryption, mongo);
    }

    @Test
    public void shouldSendEncryptedData() throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException {
        WebTarget webTarget = mock(WebTarget.class);
        Builder builder = mock(Builder.class);
        SymmetricDocument document = new SymmetricDocument(new byte[]{});

        StartMessage startMessage = new StartMessage("http://any.client.host", "some secret message", "key");

        when(client.target(anyString())).thenReturn(webTarget);
        when(webTarget.path(anyString())).thenReturn(webTarget);
        when(webTarget.request(anyString())).thenReturn(builder);
        when(builder.post(any(Entity.class), eq(SymmetricDocument.class))).thenReturn(document);

        protocol.sendEcodedMessage(startMessage);

        verify(client).target("http://any.client.host");
        verify(symmetricEncryption).encode("some secret message", "key");
    }

    @Test
    public void shouldSaveEncryptedData() {
        byte[] anyMessage = {};
        EncodedMessage encodedMessage = new EncodedMessage(anyMessage);

        protocol.saveEcodedMessage(encodedMessage);

        verify(mongo).insert(any(SymmetricDocument.class));
    }
}
