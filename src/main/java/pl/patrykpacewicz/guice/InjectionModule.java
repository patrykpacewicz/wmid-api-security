package pl.patrykpacewicz.guice;

import com.google.inject.*;
import com.google.inject.name.Names;
import com.mongodb.Mongo;

import org.springframework.data.mongodb.core.*;

import java.net.UnknownHostException;
import java.security.*;
import javax.crypto.*;
import javax.inject.Singleton;
import javax.ws.rs.client.*;

import pl.patrykpacewicz.electronicvote.VoteEndpoint;
import pl.patrykpacewicz.security.commitmentscheme.rest.SymmetricCommitmentResource;
import pl.patrykpacewicz.security.secretsharing.rest.SecretSharingResource;
import pl.patrykpacewicz.security.voting.rest.ElectionAuthorityResource;
import pl.patrykpacewicz.security.voting.rest.VoterResource;
import pl.patrykpacewicz.security.voting.rest.VotingCenterResource;

public class InjectionModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(String.class).annotatedWith(Names.named("hostname")).toInstance("http://localhost:8882/api");
        bind(SecretSharingResource.class);
        bind(SymmetricCommitmentResource.class);
        bind(VoterResource.class);
        bind(ElectionAuthorityResource.class);
        bind(VotingCenterResource.class);
        bind(VoteEndpoint.class);
    }

    @Provides
    protected Client provideClient() {
        return ClientBuilder.newClient();
    }

    @Provides @Singleton
    protected MongoOperations provideMongoOperations() throws UnknownHostException {
        return new MongoTemplate(new Mongo(), "database");
    }

    @Provides
    private Cipher provideCipher() throws NoSuchPaddingException, NoSuchAlgorithmException {
        return Cipher.getInstance("Blowfish");
    }

    @Provides
    private MessageDigest provideMessageDigest() throws NoSuchAlgorithmException {
        return MessageDigest.getInstance("SHA-1");
    }
}
