package pl.patrykpacewicz.security.commitmentscheme;

import org.springframework.data.mongodb.core.MongoOperations;

import java.security.InvalidKeyException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import pl.patrykpacewicz.security.commitmentscheme.document.SymmetricDocument;
import pl.patrykpacewicz.security.commitmentscheme.model.EncodedMessage;
import pl.patrykpacewicz.security.commitmentscheme.model.StartMessage;
import pl.patrykpacewicz.security.util.SymmetricKeyEncryption;

public class SymmetricProtocol {
    private static final String targetAddress = "commitment/symmetric/receive";

    private final Client client;
    private final SymmetricKeyEncryption symmetricEncryption;
    private final MongoOperations mongo;

    @Inject
    public SymmetricProtocol(Client client, SymmetricKeyEncryption symmetricEncryption, MongoOperations mongo) {
        this.client              = client;
        this.symmetricEncryption = symmetricEncryption;
        this.mongo               = mongo;
    }

    public SymmetricDocument sendEcodedMessage(StartMessage startMessage)
            throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException
    {
        byte[] encodeMessage = symmetricEncryption.encode(startMessage.getMessage(), startMessage.getKey());
        return clientRequest(startMessage.getClientHost(), new EncodedMessage(encodeMessage));
    }

    public SymmetricDocument saveEcodedMessage(EncodedMessage encodedMessage) {
        SymmetricDocument objectToSave = new SymmetricDocument(encodedMessage.getData());
        mongo.insert(objectToSave);
        return objectToSave;
    }

    public Boolean validateMessage(SymmetricDocument symmetricDocument)
            throws BadPaddingException, InvalidKeyException, IllegalBlockSizeException
    {
        SymmetricDocument document = mongo.findById(symmetricDocument.getId(), SymmetricDocument.class);

        if (document == null) {
            return false;
        }

        if (!document.getId().equals(symmetricDocument.getId())) {
            return false;
        }

        if (!Arrays.equals(document.getEncodedMessage(), symmetricDocument.getEncodedMessage())) {
            return false;
        }

        byte[] encode = symmetricEncryption.encode(symmetricDocument.getMessage(), symmetricDocument.getKey());
        if (!Arrays.equals(encode, symmetricDocument.getEncodedMessage())) {
            return false;
        }

        String decode = symmetricEncryption.decode(symmetricDocument.getEncodedMessage(), symmetricDocument.getKey());
        if (!decode.equals(symmetricDocument.getMessage())) {
            return false;
        }

        mongo.save(symmetricDocument);
        return true;
    }

    private SymmetricDocument clientRequest(String clientHost, EncodedMessage encodedMessage) {
        return client
                .target(clientHost)
                .path(targetAddress)
                .request(MediaType.APPLICATION_JSON)
                .post(
                        Entity.json(encodedMessage),
                        SymmetricDocument.class
                );
    }
}
