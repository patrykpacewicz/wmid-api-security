package pl.patrykpacewicz.security.commitmentscheme.document;

import org.springframework.data.annotation.Id;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SymmetricDocument {
    @Id
    private String id;
    private byte[] encodedMessage;
    private String message;
    private String key;

    protected SymmetricDocument() {}

    public SymmetricDocument(byte[] encodedMessage, String message, String key) {
        this.encodedMessage = encodedMessage;
        this.message = message;
        this.key = key;
    }

    public SymmetricDocument(byte[] encodedMessage) {
        this.encodedMessage = encodedMessage;
    }

    public String getId() {
        return id;
    }

    public byte[] getEncodedMessage() {
        return encodedMessage;
    }

    public String getMessage() {
        return message;
    }

    public String getKey() {
        return key;
    }
}
