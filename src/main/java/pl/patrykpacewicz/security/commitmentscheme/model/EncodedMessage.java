package pl.patrykpacewicz.security.commitmentscheme.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EncodedMessage {
    private byte[] data;

    protected EncodedMessage() {}

    public EncodedMessage(byte[] data) {
        this.data = data;
    }

    public byte[] getData() {
        return data;
    }
}
