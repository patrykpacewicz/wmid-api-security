package pl.patrykpacewicz.security.commitmentscheme.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StartMessage {
    private String clientHost;
    private String message;
    private String key;

    protected StartMessage() {}

    public StartMessage(String clientHost, String message, String key) {
        this.clientHost = clientHost;
        this.message    = message;
        this.key        = key;
    }

    public String getKey() {
        return key;
    }

    public String getMessage() {
        return message;
    }

    public String getClientHost() {
        return clientHost;
    }
}
