package pl.patrykpacewicz.security.commitmentscheme.rest;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.springframework.data.mongodb.core.MongoOperations;
import pl.patrykpacewicz.security.commitmentscheme.SymmetricProtocol;
import pl.patrykpacewicz.security.commitmentscheme.document.SymmetricDocument;
import pl.patrykpacewicz.security.commitmentscheme.model.EncodedMessage;
import pl.patrykpacewicz.security.commitmentscheme.model.StartMessage;

import java.util.List;

@Path("commitment/symmetric")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class SymmetricCommitmentResource {
    private final SymmetricProtocol protocol;
    private final MongoOperations mongo;

    @Inject
    public SymmetricCommitmentResource(SymmetricProtocol protocol, MongoOperations mongo) {
        this.protocol = protocol;
        this.mongo = mongo;
    }

    @GET
    public Response get () {
        List<SymmetricDocument> documents = mongo.findAll(SymmetricDocument.class);
        return Response.ok().entity(documents).build();
    }

    @GET @Path("item/{id}")
    public Response getItem (@PathParam("id") String id) {
        SymmetricDocument document = mongo.findById(id, SymmetricDocument.class);
        return Response.ok().entity(document).build();
    }

    @POST @Path("send")
    public Response send (StartMessage startMessage) {
        try {
            SymmetricDocument symmetricDocument = protocol.sendEcodedMessage(startMessage);
            return Response.ok().entity(symmetricDocument).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }

    @POST @Path("receive")
    public Response receive (EncodedMessage encodedMessage) {
        SymmetricDocument symmetricDocument = protocol.saveEcodedMessage(encodedMessage);
        return Response.ok().entity(symmetricDocument).build();
    }

    @POST @Path("validate")
    public Response validate (SymmetricDocument symmetricDocument) {
        try {
            Boolean result = protocol.validateMessage(symmetricDocument);
            return Response.ok().entity(result).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }
}
