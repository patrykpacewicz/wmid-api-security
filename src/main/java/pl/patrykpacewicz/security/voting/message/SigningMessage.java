package pl.patrykpacewicz.security.voting.message;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SigningMessage {
    private byte[] blindVote;
    private String signature;

    public SigningMessage() { }

    public SigningMessage(byte[] blindVote, String signature) {
        this.blindVote = blindVote;
        this.signature = signature;
    }

    public byte[] getBlindVote() {
        return blindVote;
    }

    public String getSignature() {
        return signature;
    }
}
