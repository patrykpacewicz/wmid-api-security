package pl.patrykpacewicz.security.voting.message;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class VotingMessage {
    private String votingOption;
    private byte[] signedMessage;

    public VotingMessage() { }

    public VotingMessage(String votingOption, byte[] signedMessage) {
        this.votingOption = votingOption;
        this.signedMessage = signedMessage;
    }

    public String getVotingOption() {
        return votingOption;
    }

    public byte[] getSignedMessage() {
        return signedMessage;
    }
}
