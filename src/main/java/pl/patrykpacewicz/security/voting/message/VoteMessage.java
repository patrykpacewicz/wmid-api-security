package pl.patrykpacewicz.security.voting.message;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class VoteMessage {
    private int vote;
    private String signature;

    public VoteMessage() { }

    public VoteMessage(int vote, String signature) {
        this.vote = vote;
        this.signature = signature;
    }

    public int getVote() {
        return vote;
    }

    public String getSignature() {
        return signature;
    }
}
