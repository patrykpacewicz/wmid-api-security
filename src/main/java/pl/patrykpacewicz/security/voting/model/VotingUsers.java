package pl.patrykpacewicz.security.voting.model;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class VotingUsers {
    private Map<String, AtomicBoolean> options;

    public VotingUsers() {
        options = new HashMap<>();
        options.put("z9dcdx98-ab6e-47f4-8c35-466515abfb7d", new AtomicBoolean(false));
        options.put("fa2074af-1af8-4fa4-944c-39b0c9e0b621", new AtomicBoolean(false));
        options.put("a7b8c487-fccb-4b9a-bd3d-9966d480d9b0", new AtomicBoolean(false));
        options.put("67663fce-de12-48b9-a1fb-ce22f5290e43", new AtomicBoolean(false));
        options.put("c10a0380-6458-473b-849f-e0ceb9ae09fc", new AtomicBoolean(false));
        options.put("dc0495f0-1556-4310-8391-b291131f5219", new AtomicBoolean(false));
        options.put("1e79908d-cec9-47d6-bcd0-1a8d4e2ce6ac", new AtomicBoolean(false));
        options.put("6cbf0fde-c84e-4b12-bb56-1f24181a111c", new AtomicBoolean(false));
        options.put("aaa925ad-2c35-4f7f-bb45-452e63579b94", new AtomicBoolean(false));
        options.put("4df8078b-3f11-4ed7-ba3a-e4c2649070f8", new AtomicBoolean(false));
        options.put("8d0be4a0-6b41-11e3-981f-0800200c9a66", new AtomicBoolean(false));
    }

    public Boolean vote(String signature) {
        if (!options.containsKey(signature)){
            return false;
        }
        return options.get(signature).compareAndSet(false, true);
    }
}
