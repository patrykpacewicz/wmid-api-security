package pl.patrykpacewicz.security.voting.model;

import java.util.*;

public class VotingOptions {
    private List<String> options;

    public VotingOptions() {
        options = new LinkedList<>();
        options.add("Barack Obama");
        options.add("Mitt Romney");
    }

    public List<String> getOptions() {
        return options;
    }

    public String getOption(Integer number) {
        return options.get(number);
    }
}
