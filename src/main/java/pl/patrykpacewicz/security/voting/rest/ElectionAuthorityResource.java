package pl.patrykpacewicz.security.voting.rest;

import pl.patrykpacewicz.security.blindsignature.BlindSignatureProtocol;
import pl.patrykpacewicz.security.blindsignature.generator.RsaBlindKeyGenerator;
import pl.patrykpacewicz.security.blindsignature.key.KeyPair;
import pl.patrykpacewicz.security.voting.message.SigningMessage;
import pl.patrykpacewicz.security.voting.model.VotingUsers;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.security.NoSuchAlgorithmException;

@Path("voting/election-authority")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class ElectionAuthorityResource {
    private static KeyPair keyPair;

    private final RsaBlindKeyGenerator rsaBlindKeyGenerator;
    private final BlindSignatureProtocol blindSignatureProtocol;
    private static final VotingUsers votingUsers = new VotingUsers();

    @Inject
    public ElectionAuthorityResource(
        RsaBlindKeyGenerator rsaBlindKeyGenerator,
        BlindSignatureProtocol blindSignatureProtocol
    ) {
        this.rsaBlindKeyGenerator   = rsaBlindKeyGenerator;
        this.blindSignatureProtocol = blindSignatureProtocol;
    }

    @POST @Path("sign")
    public Response signVote(SigningMessage signingMessage) throws NoSuchAlgorithmException {
        if (!ElectionAuthorityResource.votingUsers.vote(signingMessage.getSignature())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        byte[] signedVote = blindSignatureProtocol.sign(signingMessage.getBlindVote() , getKey().getPrivateKey());
        return Response.ok().entity(signedVote).build();
    }

    @GET @Path("key")
    public Response key() throws NoSuchAlgorithmException {
        return Response.ok().entity(getKey().getPublicKey()).build();
    }

    private KeyPair getKey() throws NoSuchAlgorithmException {
        if (keyPair == null) {
            keyPair = rsaBlindKeyGenerator.generateRsaKey();
        }

        return keyPair;
    }
}
