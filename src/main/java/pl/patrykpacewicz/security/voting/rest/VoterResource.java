package pl.patrykpacewicz.security.voting.rest;

import pl.patrykpacewicz.security.blindsignature.BlindSignatureProtocol;
import pl.patrykpacewicz.security.blindsignature.generator.RsaBlindKeyGenerator;
import pl.patrykpacewicz.security.blindsignature.key.PublicKey;
import pl.patrykpacewicz.security.blindsignature.model.RsaBlindKey;
import pl.patrykpacewicz.security.voting.client.ElectionAuthorityClient;
import pl.patrykpacewicz.security.voting.client.VotingCenterClient;
import pl.patrykpacewicz.security.voting.message.SigningMessage;
import pl.patrykpacewicz.security.voting.message.VoteMessage;
import pl.patrykpacewicz.security.voting.model.VotingOptions;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;

@Path("voting/voter")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class VoterResource {
    private final RsaBlindKeyGenerator rsaBlindKeyGenerator;
    private final VotingOptions votingOptions;
    private final BlindSignatureProtocol blindSignatureProtocol;
    private final ElectionAuthorityClient client;
    private final VotingCenterClient votingCenterClient;

    @Inject
    public VoterResource(
        VotingOptions votingOptions,
        RsaBlindKeyGenerator rsaBlindKeyGenerator,
        BlindSignatureProtocol blindSignatureProtocol,
        ElectionAuthorityClient client,
        VotingCenterClient votingCenterClient
    ) {
        this.blindSignatureProtocol = blindSignatureProtocol;
        this.rsaBlindKeyGenerator = rsaBlindKeyGenerator;
        this.votingOptions = votingOptions;
        this.client = client;
        this.votingCenterClient = votingCenterClient;
    }

    @POST @Path("vote")
    public Response vote(VoteMessage vote) throws NoSuchAlgorithmException {
        String voteOption = votingOptions.getOption(vote.getVote());

        PublicKey publicKey     = client.getPublicKey();
        BigInteger shadingValue = rsaBlindKeyGenerator.generateShadingValue(publicKey);
        RsaBlindKey rsaBlindKey = new RsaBlindKey(null, publicKey, shadingValue);

        byte[] blindedVote        = blindSignatureProtocol.blindMessage(voteOption.getBytes(), rsaBlindKey);
        byte[] signedBlindMessage = client.signBlindedVote(new SigningMessage(blindedVote, vote.getSignature()));
        byte[] signedVote         = blindSignatureProtocol.unblindMessage(signedBlindMessage, rsaBlindKey);

        boolean isVoteAccepted = votingCenterClient.vote(voteOption, signedVote);

        if (!isVoteAccepted) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        return Response.ok().build();
    }
}
