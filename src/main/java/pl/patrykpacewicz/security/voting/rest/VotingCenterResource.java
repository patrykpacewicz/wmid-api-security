package pl.patrykpacewicz.security.voting.rest;

import pl.patrykpacewicz.security.blindsignature.BlindSignatureProtocol;
import pl.patrykpacewicz.security.voting.client.ElectionAuthorityClient;
import pl.patrykpacewicz.security.voting.message.VotingMessage;
import pl.patrykpacewicz.security.voting.model.VotingOptions;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Path("voting/voting-center")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class VotingCenterResource {
    private final BlindSignatureProtocol blindSignatureProtocol;
    private final ElectionAuthorityClient client;
    private final VotingOptions options;
    private static final Map<String, AtomicInteger> results = new HashMap<>();

    @Inject
    public VotingCenterResource(
        BlindSignatureProtocol blindSignatureProtocol,
        ElectionAuthorityClient client,
        VotingOptions options
    ) {
        this.blindSignatureProtocol = blindSignatureProtocol;
        this.client = client;
        this.options = options;
    }

    @POST @Path("vote")
    public Response castVote(VotingMessage votingMessage) {
        String votingOption = votingMessage.getVotingOption();
        boolean isValid = blindSignatureProtocol.verifySignedMessage(
                votingMessage.getVotingOption().getBytes(),
                votingMessage.getSignedMessage(),
                client.getPublicKey()
        );

        if (!isValid) {
            return Response.ok().entity(isValid).build();
        }

        if (!VotingCenterResource.results.containsKey(votingOption)) {
            VotingCenterResource.results.put(votingOption, new AtomicInteger(0));
        }

        VotingCenterResource.results.get(votingOption).incrementAndGet();
        return Response.ok().entity(isValid).build();
    }

    @GET @Path("options")
    public Response voteOptions() {
        return Response.ok().entity(options.getOptions()).build();
    }

    @GET @Path("vote")
    public Response voteResult() {
        return Response.ok().entity(results).build();
    }

}
