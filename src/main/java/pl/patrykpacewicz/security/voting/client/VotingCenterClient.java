package pl.patrykpacewicz.security.voting.client;

import pl.patrykpacewicz.security.voting.message.VotingMessage;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

public class VotingCenterClient {
    private final Client client;
    private final String hostname;

    @Inject
    public VotingCenterClient(Client client, @Named("hostname") String hostname) {
        this.client = client;
        this.hostname = hostname;
    }

    public boolean vote(String votingOption, byte[] signedMessage) {
        VotingMessage votingMessage = new VotingMessage(votingOption, signedMessage);

        return client.target(hostname).path("voting/voting-center/vote")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(votingMessage), boolean.class);
    }
}
