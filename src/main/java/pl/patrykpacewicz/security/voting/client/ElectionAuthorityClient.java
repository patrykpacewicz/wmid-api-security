package pl.patrykpacewicz.security.voting.client;

import pl.patrykpacewicz.security.blindsignature.key.PublicKey;
import pl.patrykpacewicz.security.voting.message.SigningMessage;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

public class ElectionAuthorityClient {
    private final Client client;
    private final String hostname;

    @Inject
    public ElectionAuthorityClient(Client client, @Named("hostname") String hostname) {
        this.client = client;
        this.hostname = hostname;
    }

    public PublicKey getPublicKey() {
        return client.target(hostname).path("voting/election-authority/key")
                .request(MediaType.APPLICATION_JSON)
                .get(PublicKey.class);
    }

    public byte[] signBlindedVote(SigningMessage signingMessage) {
        return client.target(hostname).path("voting/election-authority/sign")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(signingMessage), byte[].class);
    }
}
