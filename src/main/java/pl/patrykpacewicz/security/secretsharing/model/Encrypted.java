package pl.patrykpacewicz.security.secretsharing.model;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Encrypted {
    private List<byte[]> data;

    protected Encrypted() { }

    public Encrypted(List<byte[]> data) {
        this.data = data;
    }

    public List<byte[]> getData() {
        return data;
    }
}
