package pl.patrykpacewicz.security.secretsharing.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Decrypted {
    private String  message;
    private Integer number;

    protected Decrypted() { }

    public Decrypted(String message, Integer number) {
        this.message = message;
        this.number = number;
    }

    public String getMessage() {
        return message;
    }

    public Integer getNumber() {
        return number;
    }
}
