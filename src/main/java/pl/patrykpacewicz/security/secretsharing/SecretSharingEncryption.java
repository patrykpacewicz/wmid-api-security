package pl.patrykpacewicz.security.secretsharing;

import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.inject.Inject;

import pl.patrykpacewicz.security.secretsharing.model.Decrypted;
import pl.patrykpacewicz.security.secretsharing.model.Encrypted;
import pl.patrykpacewicz.security.util.RandomGenerator;
import pl.patrykpacewicz.security.util.Xor;

public class SecretSharingEncryption {
    private final Xor xor;
    private final RandomGenerator random;

    @Inject
    public SecretSharingEncryption(Xor xor, RandomGenerator random) {
        this.xor    = xor;
        this.random = random;
    }

    public Encrypted encode (Decrypted decrypted) {
        Integer messageLength = decrypted.getMessage().length();
        byte[] messageBytes   = decrypted.getMessage().getBytes();
        Integer partsNumber   = decrypted.getNumber() - 1;

        List<byte[]> bytesList = random.generateBytesList(messageLength, partsNumber);

        bytesList.add(messageBytes);
        bytesList.add(xor.xor(bytesList));
        bytesList.remove(messageBytes);

        return new Encrypted(bytesList);
    }

    public Decrypted decode (Encrypted encrypted) throws UnsupportedEncodingException {
        String message = new String(xor.xor(encrypted.getData()), "UTF-8");
        Integer partsNumber = encrypted.getData().size();

        return new Decrypted(message, partsNumber);
    }
}
