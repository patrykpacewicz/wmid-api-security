package pl.patrykpacewicz.security.secretsharing.rest;

import java.io.UnsupportedEncodingException;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import pl.patrykpacewicz.security.secretsharing.SecretSharingEncryption;
import pl.patrykpacewicz.security.secretsharing.model.Decrypted;
import pl.patrykpacewicz.security.secretsharing.model.Encrypted;

@Path("secret-sharing")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class SecretSharingResource {
    private final SecretSharingEncryption encryption;

    @Inject
    public SecretSharingResource(SecretSharingEncryption encryption) {
        this.encryption = encryption;
    }

    @POST @Path("encode")
    public Response encode (Decrypted decrypted) {
        Encrypted encrypted = encryption.encode(decrypted);
        return Response.ok().entity(encrypted).build();
    }

    @POST @Path("decode")
    public Response decode (Encrypted encrypted) {
        try {
            Decrypted decrypted = encryption.decode(encrypted);
            return Response.ok().entity(decrypted).build();
        } catch (UnsupportedEncodingException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
    }
}
