package pl.patrykpacewicz.security.util;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.inject.Inject;

public class RandomGenerator {
    private final Random random;

    @Inject
    public RandomGenerator(Random random) {
        this.random = random;
    }

    public List<byte[]> generateBytesList(Integer length, Integer number) {
        final ArrayList<byte[]> randomList = Lists.newArrayList();
        byte[] randomBytes = new byte[length];

        for (int i = 0; i < number; i++) {
            random.nextBytes(randomBytes);
            randomList.add(randomBytes.clone());
        }

        return randomList;
    }
}
