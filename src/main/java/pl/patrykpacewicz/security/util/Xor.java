package pl.patrykpacewicz.security.util;

import com.google.common.collect.Lists;
import java.util.List;

public class Xor {
    public byte[] xor(List<byte[]> inputList) {
        List<byte[]> xorList = Lists.newArrayList(inputList);

        byte[] result = xorList.remove(0);
        for (byte[] nextElement : xorList) {
            result = xor(result, nextElement);
        }

        return result;
    }

    public byte[] xor(byte[] data1, byte[] data2) {
        if (data1.length < data2.length) {
            return xorWithKey(data2, data1);
        }
        return xorWithKey(data1, data2);
    }

    private byte[] xorWithKey(byte[] data, byte[] key) {
        final byte[] result = new byte[data.length];

        for (int i = 0; i < data.length; i++) {
            result[i] = (byte) (data[i] ^ key[i % key.length]);
        }

        return result;
    }
}
