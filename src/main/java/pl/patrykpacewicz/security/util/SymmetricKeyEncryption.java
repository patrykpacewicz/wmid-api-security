package pl.patrykpacewicz.security.util;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

import javax.inject.Inject;
import javax.inject.Named;

public class SymmetricKeyEncryption {
    private final Cipher cipher;
    private final MessageDigest digest;

    @Inject
    public SymmetricKeyEncryption(Cipher cipher, MessageDigest digest) {
        this.cipher = cipher;
        this.digest = digest;
    }

    public byte[] encode(String message, String key)
            throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException
    {
        cipher.init(Cipher.ENCRYPT_MODE, createKey(key));
        return cipher.doFinal(message.getBytes());
    }

    public String decode(byte[] data, String key)
            throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException
    {
        cipher.init(Cipher.DECRYPT_MODE, createKey(key));
        return new String(cipher.doFinal(data));
    }

    private SecretKeySpec createKey(String key) {
        byte[] digestKey = digest.digest(key.getBytes());
        byte[] aesKey = Arrays.copyOf(digestKey, 16);

        return new SecretKeySpec(aesKey, cipher.getAlgorithm());
    }
}
