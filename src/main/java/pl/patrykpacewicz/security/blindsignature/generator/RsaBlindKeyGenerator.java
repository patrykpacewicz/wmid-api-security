package pl.patrykpacewicz.security.blindsignature.generator;

import com.google.inject.Inject;
import pl.patrykpacewicz.security.blindsignature.key.KeyPair;
import pl.patrykpacewicz.security.blindsignature.key.PrivateKey;
import pl.patrykpacewicz.security.blindsignature.key.PublicKey;
import pl.patrykpacewicz.security.blindsignature.model.RsaBlindKey;

import java.math.BigInteger;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

public class RsaBlindKeyGenerator  {
    private final SecureRandom random;

    @Inject
    public RsaBlindKeyGenerator(SecureRandom random) {
        this.random = random;
    }

    public RsaBlindKey generate() throws NoSuchAlgorithmException {
        KeyPair rsaKeyPair      = generateRsaKey();
        BigInteger shadingValue = generateShadingValue(rsaKeyPair.getPublicKey());

        return new RsaBlindKey(rsaKeyPair.getPrivateKey(), rsaKeyPair.getPublicKey(), shadingValue);
    }

    public KeyPair generateRsaKey() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(1024);
        return mapToKeyPair(keyPairGenerator.genKeyPair());
    }

    private KeyPair mapToKeyPair(java.security.KeyPair rsaKeyPair) {
        RSAPrivateKey aPrivate = (RSAPrivateKey) rsaKeyPair.getPrivate();
        RSAPublicKey aPublic = (RSAPublicKey) rsaKeyPair.getPublic();

        return new KeyPair(
            new PublicKey(aPublic.getPublicExponent(), aPublic.getModulus()),
            new PrivateKey(aPrivate.getPrivateExponent(), aPrivate.getModulus())
        );
    }

    public BigInteger generateShadingValue(PublicKey publicKey) {
        BigInteger modulus = publicKey.getModulus();
        BigInteger shadingValue = new BigInteger(modulus.toByteArray());

        while (modulus.gcd(shadingValue).intValue() != 1) {
            byte[] bytesData = new byte[shadingValue.toByteArray().length];
            this.random.nextBytes(bytesData);
            shadingValue = new BigInteger(bytesData);
        }

        return shadingValue;
    }
}
