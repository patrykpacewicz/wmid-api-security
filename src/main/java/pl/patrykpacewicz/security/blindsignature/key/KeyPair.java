package pl.patrykpacewicz.security.blindsignature.key;

import javax.xml.bind.annotation.*;

@XmlRootElement
public class KeyPair {
    private PrivateKey privateKey;
    private PublicKey publicKey;

    public KeyPair () {}

    public KeyPair(PublicKey publicKey, PrivateKey privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }
}
