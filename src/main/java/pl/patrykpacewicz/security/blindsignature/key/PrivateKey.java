package pl.patrykpacewicz.security.blindsignature.key;

import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigInteger;

@XmlRootElement
public class PrivateKey {
    private BigInteger privateExponent;
    private BigInteger modulus;

    public PrivateKey () {}

    public PrivateKey(BigInteger privateExponent, BigInteger modulus) {
        this.privateExponent = privateExponent;
        this.modulus = modulus;
    }

    public BigInteger getPrivateExponent() {
        return privateExponent;
    }

    public BigInteger getModulus() {
        return modulus;
    }
}
