package pl.patrykpacewicz.security.blindsignature.key;

import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigInteger;

@XmlRootElement
public class PublicKey {
    private BigInteger publicExponent;
    private BigInteger modulus;

    public PublicKey () {}

    public PublicKey(BigInteger publicExponent, BigInteger modulus) {
        this.publicExponent = publicExponent;
        this.modulus = modulus;
    }

    public BigInteger getPublicExponent() {
        return publicExponent;
    }

    public BigInteger getModulus() {
        return modulus;
    }
}
