package pl.patrykpacewicz.security.blindsignature.model;

import pl.patrykpacewicz.security.blindsignature.key.PrivateKey;
import pl.patrykpacewicz.security.blindsignature.key.PublicKey;

import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigInteger;

@XmlRootElement
public class RsaBlindKey {
    private final PrivateKey rsaPrivateKey;
    private final PublicKey  rsaPublicKey;
    private final BigInteger    shadingValue;

    public RsaBlindKey(PrivateKey rsaPrivateKey, PublicKey rsaPublicKey, BigInteger shadingValue) {
        this.rsaPrivateKey = rsaPrivateKey;
        this.rsaPublicKey = rsaPublicKey;
        this.shadingValue = shadingValue;
    }

    public PrivateKey getRsaPrivateKey() {
        return rsaPrivateKey;
    }

    public PublicKey getRsaPublicKey() {
        return rsaPublicKey;
    }

    public BigInteger getShadingValue() {
        return shadingValue;
    }
}
