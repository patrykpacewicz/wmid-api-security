package pl.patrykpacewicz.security.blindsignature;

import pl.patrykpacewicz.security.blindsignature.key.PrivateKey;
import pl.patrykpacewicz.security.blindsignature.key.PublicKey;
import pl.patrykpacewicz.security.blindsignature.model.RsaBlindKey;

import java.math.BigInteger;

public class BlindSignatureProtocol {
    public byte[] blindMessage(byte[] data, RsaBlindKey key) {
        BigInteger message      = new BigInteger(data);
        BigInteger exponent     = key.getRsaPublicKey().getPublicExponent();
        BigInteger modulus      = key.getRsaPublicKey().getModulus();
        BigInteger shadingValue = key.getShadingValue();

        return message.multiply(shadingValue.modPow(exponent, modulus)).mod(modulus).toByteArray();
    }

    public byte[] sign(byte[] data, PrivateKey key) {
        BigInteger blindMessage = new BigInteger(data);
        BigInteger exponent     = key.getPrivateExponent();
        BigInteger modulus      = key.getModulus();

        return blindMessage.modPow(exponent, modulus).toByteArray();
    }

    public byte[] unblindMessage(byte[] data, RsaBlindKey key) {
        BigInteger signedBlindMessage = new BigInteger(data);
        BigInteger shadingValue       = key.getShadingValue();
        BigInteger modulus            = key.getRsaPublicKey().getModulus();

        return shadingValue.modPow(BigInteger.ONE.negate(), modulus)
                .multiply(signedBlindMessage).mod(modulus).toByteArray();
    }

    public boolean verifySignedMessage(byte[] message, byte[] signedMessage, PublicKey key) {
        BigInteger messageI       = new BigInteger(message);
        BigInteger signedMessageI = new BigInteger(signedMessage);
        BigInteger exponent       = key.getPublicExponent();
        BigInteger modulus        = key.getModulus();

        return messageI.equals(signedMessageI.modPow(exponent, modulus));
    }
}
