package pl.patrykpacewicz.electronicvote.client;

import pl.patrykpacewicz.electronicvote.messages.Authentication;
import pl.patrykpacewicz.electronicvote.messages.VoterVotes;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

public class VoterClient {
    private final Client client;
    private final String hostname;

    @Inject
    public VoterClient(Client client, @Named("hostname") String hostname) {
        this.client = client;
        this.hostname = hostname;
    }

    public VoterVotes signVoterVotes(Authentication authentication) {
        return client.target(hostname).path("vote/voter/sign-votes")
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.json(authentication), VoterVotes.class);
    }

}
