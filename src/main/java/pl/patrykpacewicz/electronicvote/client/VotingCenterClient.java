package pl.patrykpacewicz.electronicvote.client;

import pl.patrykpacewicz.electronicvote.messages.FinalVote;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;

public class VotingCenterClient {
    private final Client client;
    private final String hostname;

    @Inject
    public VotingCenterClient(Client client, @Named("hostname") String hostname) {
        this.client = client;
        this.hostname = hostname;
    }

    public Response sendNewUuid(String uuid) {
        return client.target(hostname).path("vote/center/add-uuid")
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.json(uuid));
    }

    public List<String> getVotes() {
        return client.target(hostname).path("vote/center/votes")
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .get((new LinkedList<String>()).getClass());
    }

    public Response sendFinalVote(FinalVote finalVote) {
        return client.target(hostname).path("vote/center/vote")
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.json(finalVote));
    }
}
