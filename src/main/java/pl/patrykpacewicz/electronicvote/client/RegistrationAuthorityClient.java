package pl.patrykpacewicz.electronicvote.client;

import pl.patrykpacewicz.electronicvote.messages.BytesList;
import pl.patrykpacewicz.electronicvote.messages.VoterVotesSign;
import pl.patrykpacewicz.security.blindsignature.key.PublicKey;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

public class RegistrationAuthorityClient {
    private final Client client;
    private final String hostname;

    @Inject
    public RegistrationAuthorityClient(Client client, @Named("hostname") String hostname) {
        this.client = client;
        this.hostname = hostname;
    }

    public PublicKey getPublicKey() {
        return client.target(hostname).path("vote/auth/key")
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .get(PublicKey.class);
    }

    public BytesList signVoterVotes(VoterVotesSign votes) {
        return client.target(hostname).path("vote/auth/sign")
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.json(votes), BytesList.class);
    }
}
