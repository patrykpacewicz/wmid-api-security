package pl.patrykpacewicz.electronicvote;

import pl.patrykpacewicz.electronicvote.rest.RegistrationAuthorityResource;
import pl.patrykpacewicz.electronicvote.rest.VoterResource;
import pl.patrykpacewicz.electronicvote.rest.VotingCenterResource;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("vote")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class VoteEndpoint {

    private final RegistrationAuthorityResource registrationAuthorityResource;
    private final VoterResource voterResource;
    private final VotingCenterResource votingCenterResource;

    @Inject
    public VoteEndpoint (
        RegistrationAuthorityResource registrationAuthorityResource,
        VoterResource voterResource,
        VotingCenterResource votingCenterResource
    ) {
        this.registrationAuthorityResource = registrationAuthorityResource;
        this.voterResource = voterResource;
        this.votingCenterResource = votingCenterResource;
    }

    @Path("auth")
    public RegistrationAuthorityResource getRegistrationAuthorityResource() {
        return registrationAuthorityResource;
    }

    @Path("voter")
    public VoterResource getVoterResource() {
        return voterResource;
    }

    @Path("center")
    public VotingCenterResource getVotingCenterResource() {
        return votingCenterResource;
    }
}
