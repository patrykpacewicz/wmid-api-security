package pl.patrykpacewicz.electronicvote.entity.fakedata;

import java.util.LinkedList;
import java.util.List;

public class Votes {
    private List<String> votes = new LinkedList<>();

    {
        votes.add("Vote option nr 1");
        votes.add("Vote option nr 2");
        votes.add("Vote option nr 3");
        votes.add("Vote option nr 4");
        votes.add("Vote option nr 5");
    }

    public List<String> getVotes() {
        return votes;
    }
}
