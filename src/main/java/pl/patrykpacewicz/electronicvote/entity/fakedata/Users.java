package pl.patrykpacewicz.electronicvote.entity.fakedata;

import pl.patrykpacewicz.electronicvote.entity.User;

import java.util.LinkedList;
import java.util.List;

public class Users {
    private List<User> users = new LinkedList<>();

    {
        users.add(new User("user.user.0", "haslo123"));
        users.add(new User("user.user.1", "haslo123"));
        users.add(new User("user.user.2", "haslo123"));
        users.add(new User("user.user.3", "haslo123"));
        users.add(new User("user.user.4", "haslo123"));
        users.add(new User("user.user.5", "haslo123"));
        users.add(new User("user.user.6", "haslo123"));
        users.add(new User("user.user.7", "haslo123"));
        users.add(new User("user.user.8", "haslo123"));
        users.add(new User("user.user.9", "haslo123"));
    }

    public List<User> getAllUsers() {
        return users;
    }

    public User findUser(String username, String password) {
        for (User user : users) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public void removeUser(String username) {
        for (User user : users) {
            if (user.getUsername().equals(username)) {
                users.remove(user);
                break;
            }
        }
    }
}
