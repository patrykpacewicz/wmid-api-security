package pl.patrykpacewicz.electronicvote.messages;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class BytesList {
    private List<byte[]> data;

    public BytesList() {}

    public BytesList(List<byte[]> data) {
        this.data = data;
    }

    public List<byte[]> getData() {
        return data;
    }

    public void setData(List<byte[]> data) {
        this.data = data;
    }
}
