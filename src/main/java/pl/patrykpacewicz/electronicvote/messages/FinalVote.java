package pl.patrykpacewicz.electronicvote.messages;

public class FinalVote {
    private String uuid;
    private String vote;
    private byte[] signedVote;

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

    public String getVote() {
        return vote;
    }

    public void setSignedVote(byte[] signedVote) {
        this.signedVote = signedVote;
    }

    public byte[] getSignedVote() {
        return signedVote;
    }
}
