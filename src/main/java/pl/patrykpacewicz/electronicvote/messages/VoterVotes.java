package pl.patrykpacewicz.electronicvote.messages;

import java.util.List;

public class VoterVotes {
    private String uuid;
    private BytesList signedVotes;
    private List<String> votes;

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setSignedVotes(BytesList signedVotes) {
        this.signedVotes = signedVotes;
    }

    public BytesList getSignedVotes() {
        return signedVotes;
    }

    public void setVotes(List<String> votes) {
        this.votes = votes;
    }

    public List<String> getVotes() {
        return votes;
    }
}
