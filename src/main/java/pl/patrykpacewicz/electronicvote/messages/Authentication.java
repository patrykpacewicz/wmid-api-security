package pl.patrykpacewicz.electronicvote.messages;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Authentication {
    private String username;
    private String password;

    public Authentication() {}

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
