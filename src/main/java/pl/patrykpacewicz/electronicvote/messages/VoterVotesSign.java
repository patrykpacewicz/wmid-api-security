package pl.patrykpacewicz.electronicvote.messages;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement
public class VoterVotesSign {
    private String username;
    private String password;
    private String uuid;
    private List<byte[]> signedVotes = new LinkedList<>();

    public VoterVotesSign() {}

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setSignedVotes(List<byte[]> signedVotes) {
        this.signedVotes = signedVotes;
    }

    public List<byte[]> getSignedVotes() {
        return signedVotes;
    }
}
