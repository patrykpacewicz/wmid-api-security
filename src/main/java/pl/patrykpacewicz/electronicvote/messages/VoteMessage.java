package pl.patrykpacewicz.electronicvote.messages;

public class VoteMessage {
    private String uuid;
    private String vote;

    public VoteMessage() {}

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }
}
