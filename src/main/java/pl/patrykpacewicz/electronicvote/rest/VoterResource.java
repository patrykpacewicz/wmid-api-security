package pl.patrykpacewicz.electronicvote.rest;

import pl.patrykpacewicz.electronicvote.client.RegistrationAuthorityClient;
import pl.patrykpacewicz.electronicvote.client.VotingCenterClient;
import pl.patrykpacewicz.electronicvote.messages.*;
import pl.patrykpacewicz.security.blindsignature.BlindSignatureProtocol;
import pl.patrykpacewicz.security.blindsignature.generator.RsaBlindKeyGenerator;
import pl.patrykpacewicz.security.blindsignature.key.PublicKey;
import pl.patrykpacewicz.security.blindsignature.model.RsaBlindKey;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.util.*;

public class VoterResource {
    private final RsaBlindKeyGenerator rsaBlindKeyGenerator;
    private final BlindSignatureProtocol blindSignatureProtocol;
    private final RegistrationAuthorityClient registrationAuthorityClient;
    private final VotingCenterClient votingCenterClient;

    private static Map<String, VoterVotes> voterVotesList = new HashMap<>();

    @Inject
    public VoterResource (
        RsaBlindKeyGenerator rsaBlindKeyGenerator,
        BlindSignatureProtocol blindSignatureProtocol,
        RegistrationAuthorityClient registrationAuthorityClient,
        VotingCenterClient votingCenterClient
    ) {
        this.rsaBlindKeyGenerator = rsaBlindKeyGenerator;
        this.blindSignatureProtocol = blindSignatureProtocol;
        this.registrationAuthorityClient = registrationAuthorityClient;
        this.votingCenterClient = votingCenterClient;
    }

    @POST @Path("sign-votes")
    public Response getSignedVoteOptions(Authentication authentication) {
        String uuid = UUID.randomUUID().toString();

        VoterVotesSign voterVotesSign = new VoterVotesSign();
        voterVotesSign.setUsername(authentication.getUsername());
        voterVotesSign.setPassword(authentication.getPassword());
        voterVotesSign.setUuid(uuid);

        RsaBlindKey rsaBlindKey = getRsaBlindKey();

        List<String> votes = votingCenterClient.getVotes();
        for (String vote : votes) {
            String newVote = vote + "||" + uuid;
            voterVotesSign.getSignedVotes().add(blindSignatureProtocol.blindMessage(newVote.getBytes(), rsaBlindKey));
        }

        BytesList signBlindVotes = registrationAuthorityClient.signVoterVotes(voterVotesSign);

        BytesList singVotes = new BytesList(new LinkedList<byte[]>());
        for (byte[] signBlindVote : signBlindVotes.getData()) {
            singVotes.getData().add(blindSignatureProtocol.unblindMessage(signBlindVote, rsaBlindKey));
        }

        VoterVotes voterVotes = new VoterVotes();
        voterVotes.setUuid(uuid);
        voterVotes.setSignedVotes(singVotes);
        voterVotes.setVotes(votes);

        voterVotesList.put(uuid, voterVotes);
        return Response.ok().entity(voterVotes).build();
    }

    @POST @Path("vote")
    public Response vote(VoteMessage vote) {
        if (!voterVotesList.containsKey(vote.getUuid())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        VoterVotes voterVotes = voterVotesList.get(vote.getUuid());

        if (!voterVotes.getVotes().contains(vote.getVote())) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        int indexOfVote = voterVotes.getVotes().indexOf(vote.getVote());
        byte[] signedVote = voterVotes.getSignedVotes().getData().get(indexOfVote);

        FinalVote finalVote = new FinalVote();
        finalVote.setUuid(vote.getUuid());
        finalVote.setVote(vote.getVote());
        finalVote.setSignedVote(signedVote);

        Response response = votingCenterClient.sendFinalVote(finalVote);
        if (!response.getStatusInfo().equals(Response.Status.OK)) {
            return Response.status(response.getStatusInfo()).build();
        }

        voterVotesList.remove(vote.getUuid());
        return Response.ok().build();
    }

    private RsaBlindKey getRsaBlindKey() {
        PublicKey publicKey = registrationAuthorityClient.getPublicKey();
        BigInteger shadingValue = rsaBlindKeyGenerator.generateShadingValue(publicKey);
        return new RsaBlindKey(null, publicKey, shadingValue);
    }
}
