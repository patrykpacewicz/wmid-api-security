package pl.patrykpacewicz.electronicvote.rest;

import pl.patrykpacewicz.electronicvote.client.VotingCenterClient;
import pl.patrykpacewicz.electronicvote.entity.User;
import pl.patrykpacewicz.electronicvote.entity.fakedata.Users;
import pl.patrykpacewicz.electronicvote.messages.BytesList;
import pl.patrykpacewicz.electronicvote.messages.VoterVotesSign;
import pl.patrykpacewicz.security.blindsignature.BlindSignatureProtocol;
import pl.patrykpacewicz.security.blindsignature.generator.RsaBlindKeyGenerator;
import pl.patrykpacewicz.security.blindsignature.key.KeyPair;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;

public class RegistrationAuthorityResource {
    private final RsaBlindKeyGenerator rsaBlindKeyGenerator;
    private final BlindSignatureProtocol blindSignatureProtocol;
    private final VotingCenterClient votingCenterClient;
    private static Users users = new Users();
    private static KeyPair keyPair;

    @Inject
    public RegistrationAuthorityResource (
        RsaBlindKeyGenerator rsaBlindKeyGenerator,
        BlindSignatureProtocol blindSignatureProtocol,
        VotingCenterClient votingCenterClient
    ) {
        this.rsaBlindKeyGenerator   = rsaBlindKeyGenerator;
        this.blindSignatureProtocol = blindSignatureProtocol;
        this.votingCenterClient     = votingCenterClient;
    }

    @POST @Path("sign")
    public Response sign(VoterVotesSign votes) {
        User user = users.findUser(votes.getUsername(), votes.getPassword());
        if (user == null) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        votingCenterClient.sendNewUuid(votes.getUuid());
        users.removeUser(votes.getUsername());

        BytesList signedList = new BytesList(new LinkedList<byte[]>());
        for (byte[] bytes : votes.getSignedVotes()) {
            signedList.getData().add(blindSignatureProtocol.sign(bytes, getKey().getPrivateKey()));
        }

        return Response.ok().entity(signedList).build();
    }

    @GET @Path("key")
    public Response key() {
        return Response.ok().entity(getKey().getPublicKey()).build();
    }

    private KeyPair getKey() {
        if (keyPair == null) {
            try {
                keyPair = rsaBlindKeyGenerator.generateRsaKey();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        return keyPair;
    }
}
