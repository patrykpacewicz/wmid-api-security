package pl.patrykpacewicz.electronicvote.rest;

import pl.patrykpacewicz.electronicvote.client.RegistrationAuthorityClient;
import pl.patrykpacewicz.electronicvote.entity.fakedata.Votes;
import pl.patrykpacewicz.electronicvote.messages.FinalVote;
import pl.patrykpacewicz.security.blindsignature.BlindSignatureProtocol;
import pl.patrykpacewicz.security.blindsignature.key.PublicKey;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class VotingCenterResource {
    private static List<String> secureUsersUuid = new LinkedList<>();
    private final Votes votes;
    private final BlindSignatureProtocol blindSignatureProtocol;
    private final RegistrationAuthorityClient registrationAuthorityClient;
    private final Map<String, AtomicInteger> finalVotes = new HashMap<>();

    @Inject
    public VotingCenterResource (
        Votes votes,
        BlindSignatureProtocol blindSignatureProtocol,
        RegistrationAuthorityClient registrationAuthorityClient
    ) {
        this.votes = votes;
        this.blindSignatureProtocol = blindSignatureProtocol;
        this.registrationAuthorityClient = registrationAuthorityClient;

        for (String vote : votes.getVotes()) {
            finalVotes.put(vote, new AtomicInteger(0));
        }
    }

    @POST @Path("add-uuid")
    public Response addUuid(String uuid) {
        secureUsersUuid.add(uuid);
        return Response.ok().build();
    }

    @GET @Path("votes")
    public Response getVotes() {
        return Response.ok().entity(votes.getVotes()).build();
    }


    @GET @Path("final-votes")
    public Response getFinalVotes() {
        return Response.ok().entity(finalVotes).build();
    }

    @POST @Path("vote")
    public Response vote(FinalVote vote) {
        PublicKey publicKey = registrationAuthorityClient.getPublicKey();

        if (!secureUsersUuid.contains(vote.getUuid())) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        boolean isVoteValid = votes.getVotes().contains(vote.getVote());

        boolean isMessageVerify = blindSignatureProtocol.verifySignedMessage(
                (vote.getVote() + "||" + vote.getUuid()).getBytes(),
                vote.getSignedVote(),
                publicKey
        );

        if (!isVoteValid || !isMessageVerify) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        secureUsersUuid.remove(vote.getUuid());
        finalVotes.get(vote.getVote()).incrementAndGet();
        return Response.ok().build();
    }
}
